import { allure } from "allure-playwright";
import { WaitPageLocators as locators } from '../data/wait-page-data';
import { BasePage } from './base-page';
import { Links } from '../config/links';


export class WaitPage extends BasePage {
    links = new Links();
    PAGE_URL = this.links.WAITS;


    async waitAndAcceptAlert() {
        await allure.step('Ожидание и принятие Alert', async() => {
            this.page.once('dialog', async dialog => {
                await dialog.accept();
            });
            await this.page.locator(locators.ACCEPT_BTN).click();
            await this.page.waitForEvent('dialog', { timeout: 10000 });
        });
    }
}