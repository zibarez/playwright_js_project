import { allure } from "allure-playwright";
import { CalendarPageLocators, CalendarPageData as data} from '../data/calendar-page-data';
import { BasePage } from './base-page';
import { Links } from '../config/links';
import { expect } from '@playwright/test'
import { format, add } from "date-fns";


export class CalendarPage extends BasePage {
    links = new Links();
    locators = new CalendarPageLocators();
    PAGE_URL = this.links.CALENDAR;
    today = new Date();
    end;

    async chooseStartDateAsToday() {
        await allure.step('Выбор сегодняшней даты как начало интервала', async() => {
            await allure.step('Открыть календарь для ввода даты (начало интервала)', async() => {
                await this.page.locator(this.locators.DATETIME_PICKER_START).click();
            });
            await allure.step('Выбрать сегодняшний день', async() => {
                await this.page.locator(this.locators.ACTIVE_TODAY_BTN).click();
            });
            await allure.step('Закрыть календарь', async() => {
                await this.page.locator(this.locators.ACTIVE_CLOSE_BTN).click();
            });
        });
    }

    async chooseDateAfterThreeDays() {
        await allure.step('Выбрать дату через три дня для конца интервала', async() => {
            const pattern = 'MMM dd yyyy';
            this.end = add(this.today, {days: 3});
            const endData = format(this.end, pattern)
            await this.page.locator(this.locators.DATETIME_PICKER_END).click();
            const days = await this.page.locator(this.locators.ACTIVE_DAYS).all();
            for (let day of days) {
                let attribute = await day.getAttribute(data.DATE_ATTRIBUTE);
                if (attribute?.includes(endData)) {
                    await day.click();
                    break;
                }
            }
        });
    }

    async expectChosenDates() {
        await allure.step('Проверка выбранных дат', async() => {
            const text = this.page.locator(this.locators.DATE_SELECTED_TEXT);
            const pattern = 'dd-MMM-yyyy';
            const todayDate = format(this.today, pattern);
            const endDate = format(this.end, pattern);
            await expect(text).toContainText(todayDate);
            await expect(text).toContainText(endDate);
        });
    }

    async chooseTodayDateTime() {
        await allure.step('Выбор текущих даты и времени в основном календаре с помощью кнопки "Today"', async() => {
            await this.page.locator(this.locators.MAIN_TODAY_BTN).click();
        });
    }

    async increaseTimeTwoHours() {
        await allure.step('Увеличение времени на +2 часа', async() => {
            await this.page.locator(this.locators.TIMEPICKER_HOUR_INCREASE).click( { clickCount: 2 } );
        });
    }

    async expectDateTime() {
        await allure.step('Проверка установленных даты и времени', async() => {
            const text = this.page.locator(this.locators.TIME_SELECTED_TEXT);
            const pattern = 'M/d/yy, h:mm aa';
            const checkedDateTime = add(this.today, { hours: 2 });
            const formatedCheckedDateTime = format(checkedDateTime, pattern);
            await expect(text).toContainText(formatedCheckedDateTime);
        });
    }
}