import { test } from '@playwright/test';
import { WaitPage } from '../pages/wait-page';

test('Тест Calendar Page', async( {page} ) => {
    const waitPage = new WaitPage(page);
    await waitPage.navigate(waitPage.PAGE_URL);
    await waitPage.waitAndAcceptAlert();
})