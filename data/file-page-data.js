export const FilePageLocators = {
    UPLOAD: 'input[type="file"]',
}


export const  FilePageData = {
    DOWNLOAD_EXCEL_NAME: 'Download Excel',
    DOWNLOAD_PDF_NAME: 'Download Pdf',
    DOWNLOAD_TEXT_NAME: 'Download Text',
    EXCEL_NAME: 'sample.xlsx',
    PDF_NAME: 'sample.pdf',
    TEXT_NAME: 'sample.txt',
    DOWNLOAD_DIR: 'downloads',
    UPLOAD_DIR: 'upload',
    TEST_FILE_NAME: 'test_file.txt',
}
